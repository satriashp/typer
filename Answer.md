**Satria Sahputra**

##
(1) Sebutkan library apa saja yang dipakai, website library itu dimana, dan dokumentasi library itu ada dimana.

Jawaban:
* **Bootstrap**
website : `https://getbootstrap.com/` and 
documentation:  `https://getbootstrap.com/docs/3.3/`

* **Underscore.js**
website and documentation: `http://underscorejs.org/`

* **jQuery**
website: `https://jquery.com/` and documentation `http://api.jquery.com/category/version/1.0.4/`


* **jQuery Ui**
website: `http://jqueryui.com/` and documentation: `http://api.jqueryui.com/1.11/`

* **Backbone.js**
website and documentation: `http://backbonejs.org/`

##
(2) Aplikasi itu 'laggy'. Kenapa? Bagaimana cara membuat animasi lebih 'smooth'?

Jawaban: `karena interval waktu yang digunakan yaitu 100ms. Animasi akan terlihat smooth ketika mencapai 60fps. Jadi kira2 satu frame butuh interval waktu 16ms-17ms`.

##
(3) Aplikasi itu tidak akan jalan di salah satu 3 browser populer (Chrome, Firefox, Internet Explorer)? Kenapa? Solusinya hanya menghapus satu character di code, character yang mana?

Jawaban: 
##

(4).Implementasikan tombol Start, Stop, Pause, dan Resume.

Jawaban: `File typer.js line 144 s/d 183`

##
(5) Ketika ukuran window dirubah, susunan huruf yang 'terbentur' batas window menjadi tidak 1 baris. Benarkan.

Jawaban: `Listen setiap event $(window).resize() di trigger, kemudian set ulang nilai x dari model Word ny. File typer.js line 20 s/d 15.`
##

(6) Implementasikan sistem score.

Jawaban: `File typer.js line: 207 s/d 231`

##

(7) Implementasikan hukuman berupa pengurangan nilai bila salah ketik.

Jawaban: `File typer.js line: 234 s/d 244`