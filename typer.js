var Word = Backbone.Model.extend({
    move: function () {
        this.set({y: this.get('y') + this.get('speed')});
    }
});

var Words = Backbone.Collection.extend({
    model: Word
});

var WordView = Backbone.View.extend({
    initialize: function () {
        $(this.el).css({position: 'fixed'});
        var string = this.model.get('string');
        var letter_width = 25;
        var word_width = string.length * letter_width;
        var windowWidth = $(window).width();
        var self = this;

        $(window).resize(function () {
            windowWidth = $(window).width();
            if (self.model.get('x') + word_width > windowWidth) {
                self.model.set({x: windowWidth - word_width});
            }
        });

        if (this.model.get('x') + word_width > windowWidth) {
            this.model.set({x: windowWidth - word_width});
        }

        for (var i = 0; i < string.length; i++) {
            $(this.el)
                .append($('<div>')
                    .css({
                        width: letter_width + 'px',
                        padding: '5px 2px',
                        'border-radius': '4px',
                        'background-color': '#fff',
                        border: '1px solid #ccc',
                        'text-align': 'center',
                        float: 'left',
                    })
                    .text(string.charAt(i).toUpperCase()));
        }

        this.listenTo(this.model, 'remove', this.remove);

        this.render();
    },

    render: function () {
        $(this.el).css({
            top: this.model.get('y') + 'px',
            left: this.model.get('x') + 'px'
        });
        var highlight = this.model.get('highlight');
        $(this.el).find('div').each(function (index, element) {
            if (index < highlight) {
                $(element).css({'font-weight': 'bolder', 'background-color': '#aaa', color: '#fff'});
            } else {
                $(element).css({'font-weight': 'normal', 'background-color': '#fff', color: '#000'});
            }
        });
    }
});

var TyperView = Backbone.View.extend({
    events: {
        'click .start': 'startGame',
        'click .pause': 'pauseGame',
        'click .stop': 'stopGame',
        'click .resume': 'resumeGame'
    },

    initialize: function () {
        var wrapper = $('<div>')
            .css({
                position: 'fixed',
                top: '0',
                left: '0',
                width: '100%',
                height: '100%'
            });
        this.wrapper = wrapper;

        var self = this;
        var text_input = $('<input>')
            .addClass('form-control')
            .css({
                'border-radius': '4px',
                position: 'absolute',
                bottom: '0',
                'min-width': '80%',
                width: '80%',
                'margin-bottom': '10px',
                'z-index': '1000'
            }).keyup(function () {
                var words = self.model.get('words');
                for (var i = 0; i < words.length; i++) {
                    var word = words.at(i);
                    var typed_string = $(this).val();
                    var string = word.get('string');
                    if (string.toLowerCase().indexOf(typed_string.toLowerCase()) === 0) {
                        word.set({highlight: typed_string.length});
                        if (typed_string.length === string.length) {
                            $(this).val('');
                        }
                    } else {
                        Events.trigger('typoPenalty');
                        word.set({highlight: 0});
                    }
                }
            });

        var start = $('<button>')
            .addClass('btn btn-primary start')
            .css({
                'margin-right': '10px'
            })
            .append("Start");
        var pause = $('<button>')
            .addClass('btn btn-default pause')
            .css({
                'margin-right': '10px'
            })
            .append("Pause");
        var stop = $('<button>')
            .addClass('btn btn-danger stop')
            .css({
                'margin-right': '10px'
            })
            .append('Stop');
        var resume = $('<button>')
            .css({
                'margin-right': '10px'
            })
            .addClass('btn btn-warning resume')
            .append('Resume');

        var navigation = $('<div class="navigation">').css({
            'border-radius': '4px',
            position: 'absolute',
            top: '0',
            'margin-top': '10px'
        }).append(start).append(pause).append(stop).append(resume)


        $(this.el)
            .append(wrapper
                .append($('<form>')
                    .attr({
                        role: 'form'
                    })
                    .submit(function () {
                        return false;
                    })
                    .append(text_input)).append(navigation));


        text_input.css({left: ((wrapper.width() - text_input.width()) / 2) + 'px'});
        text_input.focus();

        this.listenTo(this.model, 'change', this.render);
    },

    startGame: function () {
        var model = this.model;
        model.set('state', true)
    },

    pauseGame: function () {
        var model = this.model;
        model.set('state', false)
    },

    stopGame: function () {
        var model = this.model;
        model.set('max_num_words', 0)
    },

    resumeGame: function () {
        var model = this.model;
        model.set('max_num_words', 10);
    },

    render: function () {
        var model = this.model;
        var words = model.get('words');

        for (var i = 0; i < words.length; i++) {
            var word = words.at(i);
            if (!word.get('view')) {
                var word_view_wrapper = $('<div>');
                this.wrapper.append(word_view_wrapper);
                word.set({
                    view: new WordView({
                        model: word,
                        el: word_view_wrapper
                    })
                });
            } else {
                word.get('view').render();
            }
        }
    }
});

var Score = Backbone.View.extend({

    initialize: function () {
        var model = this.model;
        var score = model.get('initialScore');
        $(this.el)
            .append($('div .navigation')
                .append('Your Score : ')
                .append($('<span>').addClass('score ')
                    .append(score)))

        this.listenTo(this.model, 'correct', this.render);
        Events.on('typoPenalty', this.penalty, this)
    },

    render: function () {
        var model = this.model;
        var score = model.get('initialScore');
        score++;

        model.set('initialScore', score);
        var result = score / 2 * model.get('correctScore');

        $('span.score').replaceWith(`<span class="score">${result}</span>`)
    },

    penalty: function () {
        var model = this.model;
        var score = model.get('initialScore') / 2 * model.get('correctScore');
        var typo = model.get('typoScore');
        typo++;

        model.set('typoScore', typo);
        var penalty = model.get('typoPenalty') * typo;
        var result = score - penalty;
        $('span.score').replaceWith(`<span class="score">${result}</span>`)
    }
});

var Typer = Backbone.Model.extend({
    defaults: {
        max_num_words: 10,
        min_distance_between_words: 50,
        words: new Words(),
        min_speed: 1,
        max_speed: 5,

        state: false,
        initialScore: 0,
        typoScore: 0,
        correctScore: 100,
        typoPenalty: 10
    },

    initialize: function () {
        new TyperView({
            model: this,
            el: $(document.body)
        });

        new Score({
            model: this,
            el: $(document.body)
        });
    },

    start: function () {
        var animation_delay = 16;
        var self = this;
        setInterval(function () {
            self.iterate();
        }, animation_delay);
    },

    iterate: function () {
        var words = this.get('words');
        if (words.length < this.get('max_num_words')) {
            var top_most_word = undefined;
            for (var i = 0; i < words.length; i++) {
                var word = words.at(i);
                if (!top_most_word) {
                    top_most_word = word;
                } else if (word.get('y') < top_most_word.get('y')) {
                    top_most_word = word;
                }
            }

            if (!top_most_word || top_most_word.get('y') > this.get('min_distance_between_words')) {
                var random_company_name_index = this.random_number_from_interval(0, company_names.length - 1);
                var string = company_names[random_company_name_index];
                var filtered_string = '';
                for (var j = 0; j < string.length; j++) {
                    if (/^[a-zA-Z()]+$/.test(string.charAt(j))) {
                        filtered_string += string.charAt(j);
                    }
                }

                var word = new Word({
                    x: this.random_number_from_interval(0, $(window).width()),
                    y: 0,
                    string: filtered_string,
                    speed: this.random_number_from_interval(this.get('min_speed'), this.get('max_speed'))
                });
                words.add(word);
            }
        }

        var words_to_be_removed = [];
        for (var i = 0; i < words.length; i++) {
            var word = words.at(i);
            if (this.get('state')) {
                word.move();
            }


            if (word.get('y') > $(window).height() || word.get('move_next_iteration')) {
                words_to_be_removed.push(word);
            }

            if (word.get('highlight') && word.get('string').length === word.get('highlight')) {
                this.trigger('correct');
                word.set({move_next_iteration: true});
            }
        }

        for (var i = 0; i < words_to_be_removed.length; i++) {
            words.remove(words_to_be_removed[i]);
        }

        this.trigger('change');
    },

    random_number_from_interval: function (min, max) {
        return Math.floor(Math.random() * (max - min + 1) + min);
    }
});

var Events = _.extend({}, Backbone.Events);